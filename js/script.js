const changeThemeBtn = document.querySelector('#switchBtn');
const changeTheme = document.querySelector(".container");
const changeAsideBtn = document.querySelector('.nav-menu-list');

let isDarkTheme;

function changeToDarkTheme() {
    changeTheme.classList.add('gradient-background');
    changeThemeBtn.innerText = 'Змінити тему на світлу';
    changeThemeBtn.style.background = 'rgb(217,221,225)'
    changeThemeBtn.style.color = 'black';
    changeAsideBtn.style.background = 'rgb(147,184,187)';
}

function changeToLightTheme() {
    changeTheme.classList.remove('gradient-background');
    changeThemeBtn.innerText = 'Змінити тему на темну';
    changeThemeBtn.style.background = 'rgb(54,117,159)';
    changeThemeBtn.style.color = 'white';
    changeAsideBtn.style.background = 'rgba(0, 0, 0, 0.0001)';
}

if (localStorage.getItem('darkTheme') === 'true') {
    isDarkTheme = true
    changeToDarkTheme()
} else {
    localStorage.setItem('darkTheme', 'false');
    isDarkTheme = false
    changeToLightTheme()
}

changeThemeBtn.addEventListener('click', () => {
    if (!isDarkTheme) {
        localStorage.setItem('darkTheme', 'true');
        isDarkTheme = true;
        changeToDarkTheme()
    } else {
        localStorage.setItem('darkTheme', 'false');
        isDarkTheme = false;
        changeToLightTheme()
    }
    /*Animation of the button*/
    changeThemeBtn.classList.add("rotate-center");
    setTimeout(function(){
        changeThemeBtn.classList.remove("rotate-center");
    }, 500);
})


